object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Cadastro de Transportadoras'
  ClientHeight = 471
  ClientWidth = 530
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 529
    Height = 472
    Caption = 'Panel1'
    TabOrder = 0
    object BT_Opcoes: TButton
      Left = 444
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Op'#231#245'es'
      TabOrder = 0
    end
    object Cancelar: TButton
      Left = 170
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Cancelar'
      Enabled = False
      TabOrder = 1
      OnClick = CancelarClick
    end
    object Excluir: TButton
      Left = 251
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Excluir'
      Enabled = False
      TabOrder = 2
      OnClick = ExcluirClick
    end
    object Gravar: TButton
      Left = 89
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Gravar'
      Enabled = False
      TabOrder = 3
      OnClick = GravarClick
    end
    object Novo: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Novo'
      TabOrder = 4
      OnClick = NovoClick
    end
    object PageControl1: TPageControl
      Left = 8
      Top = 56
      Width = 511
      Height = 401
      ActivePage = TabSheet_Cadastro
      TabOrder = 5
      object TabSheet_Consulta: TTabSheet
        Caption = 'Consulta'
        object Consulta: TLabel
          Left = 4
          Top = 19
          Width = 42
          Height = 13
          Caption = 'Consulta'
        end
        object Edit_Consulta: TEdit
          Left = 52
          Top = 16
          Width = 346
          Height = 21
          TabOrder = 0
          OnChange = ConsultarClick
        end
        object Consultar: TButton
          Left = 404
          Top = 14
          Width = 75
          Height = 25
          Caption = 'Consultar'
          Enabled = False
          TabOrder = 1
          OnClick = ConsultarClick
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 56
          Width = 503
          Height = 298
          Align = alBottom
          DataSource = DataSource1
          Options = [dgTitles, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgTitleHotTrack]
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'TR_CODIGO'
              Title.Caption = 'ID'
              Width = 31
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TR_NOME'
              Title.Caption = 'Nome'
              Width = 242
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TR_CGC'
              Title.Caption = 'CNPJ'
              Width = 113
              Visible = True
            end>
        end
        object SB_Consulta: TStatusBar
          Left = 0
          Top = 354
          Width = 503
          Height = 19
          Panels = <>
          SimplePanel = True
        end
      end
      object TabSheet_Cadastro: TTabSheet
        Caption = 'Cadastro'
        ImageIndex = 2
        object PageControl2: TPageControl
          Left = -8
          Top = -4
          Width = 511
          Height = 392
          ActivePage = TabSheet_Placas
          TabOrder = 0
          object TabSheet_Transportadora: TTabSheet
            Caption = 'Transportadora'
            object Codigo: TLabel
              Left = 26
              Top = 32
              Width = 33
              Height = 13
              Caption = 'Codigo'
            end
            object Nome: TLabel
              Left = 26
              Top = 54
              Width = 33
              Height = 13
              Caption = '*Nome'
            end
            object CNPJ: TLabel
              Left = 34
              Top = 79
              Width = 25
              Height = 13
              Caption = 'CNPJ'
            end
            object DBEdit_Nome: TDBEdit
              Left = 65
              Top = 51
              Width = 408
              Height = 21
              DataField = 'TR_NOME'
              DataSource = DataSource1
              TabOrder = 0
            end
            object DBEdit_CNPJ: TDBEdit
              Left = 65
              Top = 76
              Width = 120
              Height = 21
              DataField = 'TR_CGC'
              DataSource = DataSource1
              TabOrder = 1
            end
            object DBEdit_Codigo: TDBEdit
              Left = 65
              Top = 27
              Width = 72
              Height = 21
              Ctl3D = True
              DataField = 'TR_CODIGO'
              DataSource = DataSource1
              Enabled = False
              ParentCtl3D = False
              TabOrder = 2
            end
          end
          object TabSheet_Placas: TTabSheet
            Caption = 'Placas'
            ImageIndex = 1
            object Label_Placa: TLabel
              Left = 57
              Top = 48
              Width = 31
              Height = 13
              Caption = '*Placa'
            end
            object Label_Desc: TLabel
              Left = 42
              Top = 72
              Width = 46
              Height = 13
              Caption = 'Descri'#231#227'o'
            end
            object DBEdit_Desc: TDBEdit
              Left = 94
              Top = 69
              Width = 395
              Height = 21
              DataField = 'PL_DESCRICAO'
              DataSource = DataSource2
              TabOrder = 0
            end
            object DBEdit_Placa: TDBEdit
              Left = 94
              Top = 45
              Width = 121
              Height = 21
              DataField = 'PL_PLACA'
              DataSource = DataSource2
              TabOrder = 1
            end
            object DBGrid2: TDBGrid
              Left = 5
              Top = 124
              Width = 495
              Height = 228
              TabOrder = 2
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
            end
            object dbeTransportadorID: TDBEdit
              Left = 16
              Top = 3
              Width = 72
              Height = 21
              Ctl3D = True
              DataField = 'PL_TRANSP'
              DataSource = DataSource2
              Enabled = False
              ParentCtl3D = False
              TabOrder = 3
            end
            object dbeTransportadorNome: TDBEdit
              Left = 94
              Top = 3
              Width = 395
              Height = 21
              DataField = 'TR_NOME'
              DataSource = DataSource1
              Enabled = False
              TabOrder = 4
            end
          end
        end
      end
    end
  end
  object DataSource1: TDataSource
    DataSet = DataModule2.cdsTransportador
    Left = 344
    Top = 24
  end
  object DataSource2: TDataSource
    DataSet = DataModule2.cdsPlacas
    Left = 400
    Top = 24
  end
end
