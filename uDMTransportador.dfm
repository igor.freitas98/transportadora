object DataModule2: TDataModule2
  OldCreateOrder = False
  Height = 328
  Width = 469
  object SQLConnection1: TSQLConnection
    ConnectionName = 'FBConnection'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=Firebird'
      
        'Database=C:\Users\inter\OneDrive\Documentos\Embarcadero\Studio\t' +
        'ransportadora\Banco\DBMKEY.FDB'
      'RoleName=RoleName'
      'User_Name=SYSDBA'
      'Password=Office25'
      'ServerCharSet='
      'SQLDialect=3'
      'ErrorResourceFile='
      'LocaleCode=0000'
      'BlobSize=-1'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'IsolationLevel=ReadCommitted'
      'Trim Char=False')
    Connected = True
    Left = 48
    Top = 24
  end
  object sqldTransportador: TSQLDataSet
    CommandText = 'select * from transportadoras where tr_nome like :TNome'
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftString
        Name = 'TNome'
        ParamType = ptInput
      end>
    SQLConnection = SQLConnection1
    Left = 48
    Top = 96
    object sqldTransportadorTR_CODIGO: TIntegerField
      FieldName = 'TR_CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object sqldTransportadorTR_NOME: TStringField
      FieldName = 'TR_NOME'
      ProviderFlags = [pfInUpdate]
      Size = 40
    end
    object sqldTransportadorTR_CPF: TStringField
      FieldName = 'TR_CPF'
      ProviderFlags = [pfInUpdate]
      Size = 11
    end
    object sqldTransportadorTR_CGC: TStringField
      FieldName = 'TR_CGC'
      ProviderFlags = [pfInUpdate]
      Size = 14
    end
  end
  object dspTransportador: TDataSetProvider
    DataSet = sqldTransportador
    UpdateMode = upWhereKeyOnly
    Left = 168
    Top = 96
  end
  object cdsTransportador: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'TNome'
        ParamType = ptInput
      end>
    ProviderName = 'dspTransportador'
    Left = 280
    Top = 96
    object cdsTransportadorTR_CODIGO: TIntegerField
      FieldName = 'TR_CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object cdsTransportadorTR_NOME: TStringField
      FieldName = 'TR_NOME'
      ProviderFlags = [pfInUpdate]
      Size = 40
    end
    object cdsTransportadorTR_CGC: TStringField
      FieldName = 'TR_CGC'
      ProviderFlags = [pfInUpdate]
      EditMask = '!99.999.999/9999-99;0;'
      Size = 14
    end
  end
  object sqlGen: TSQLQuery
    MaxBlobSize = -1
    Params = <>
    SQLConnection = SQLConnection1
    Left = 368
    Top = 96
  end
  object sqldPlacas: TSQLDataSet
    CommandText = 'select * from placas'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = SQLConnection1
    Left = 48
    Top = 152
    object sqldPlacasPL_TRANSP: TIntegerField
      FieldName = 'PL_TRANSP'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object sqldPlacasPL_PLACA: TStringField
      FieldName = 'PL_PLACA'
      ProviderFlags = [pfInUpdate]
      Size = 8
    end
    object sqldPlacasPL_DESCRICAO: TStringField
      FieldName = 'PL_DESCRICAO'
      ProviderFlags = [pfInUpdate]
      Size = 60
    end
  end
  object dspPlacas: TDataSetProvider
    DataSet = sqldPlacas
    Left = 160
    Top = 152
  end
  object cdsPlacas: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspPlacas'
    Left = 280
    Top = 152
    object cdsPlacasPL_TRANSP: TIntegerField
      FieldName = 'PL_TRANSP'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object cdsPlacasPL_PLACA: TStringField
      FieldName = 'PL_PLACA'
      ProviderFlags = [pfInUpdate]
      Size = 8
    end
    object cdsPlacasPL_DESCRICAO: TStringField
      FieldName = 'PL_DESCRICAO'
      ProviderFlags = [pfInUpdate]
      Size = 60
    end
  end
end
