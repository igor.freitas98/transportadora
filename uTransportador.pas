unit uTransportador;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.StdCtrls, Vcl.ComCtrls,
  Vcl.Grids, Vcl.ExtCtrls, System.Actions, Vcl.ActnList, Vcl.Mask, Vcl.DBCtrls,
  Data.DB, Vcl.DBGrids, Data.DBXFirebird, Data.FMTBcd, Datasnap.DBClient,
  Data.SqlExpr, Datasnap.Provider;

  function ProximoID (GENERATOR: String): Integer;

type
  TForm1 = class(TForm)
    Novo: TButton;
    Gravar: TButton;
    Cancelar: TButton;
    Excluir: TButton;
    BT_Opcoes: TButton;
    PageControl1: TPageControl;
    TabSheet_Consulta: TTabSheet;
    Consulta: TLabel;
    Edit_Consulta: TEdit;
    Codigo: TLabel;
    Nome: TLabel;
    CNPJ: TLabel;
    Label_Placa: TLabel;
    Label_Desc: TLabel;
    PageControl2: TPageControl;
    TabSheet_Transportadora: TTabSheet;
    TabSheet_Placas: TTabSheet;
    TabSheet_Cadastro: TTabSheet;
    Consultar: TButton;
    Panel1: TPanel;
    DBEdit_Nome: TDBEdit;
    DBEdit_CNPJ: TDBEdit;
    DBEdit_Desc: TDBEdit;
    DBEdit_Placa: TDBEdit;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    DataSource1: TDataSource;
    SB_Consulta: TStatusBar;
    DBEdit_Codigo: TDBEdit;
    dbeTransportadorID: TDBEdit;
    dbeTransportadorNome: TDBEdit;
    DataSource2: TDataSource;
    procedure NovoClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GravarClick(Sender: TObject);
    procedure ExcluirClick(Sender: TObject);
    procedure ConsultarClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses uDMTransportador;

procedure TForm1.CancelarClick(Sender: TObject);
begin
  Novo.Enabled := true;
  Gravar.Enabled := false;
  Cancelar.Enabled := false;

  PageControl1.activepage := TabSheet_Consulta;

  begin
  if DataSource1.DataSet.State in [dsBrowse, dsEdit, dsInsert] then
  begin
    DataModule2.cdsTransportador.Cancel;
    DataModule2.cdsTransportador.ApplyUpdates(-1);
    DataModule2.cdsTransportador.Open;
    DataModule2.cdsTransportador.Close;
  end
  else
    ShowMessage('Aten��o: Favor Executar uma Opera��o.');

end;

end;

procedure TForm1.ConsultarClick(Sender: TObject);
begin
      DataModule2.cdsTransportador.Active := False;
      DataModule2.cdsTransportador.Params[0].AsString := Edit_Consulta.Text + '%';
      DataModule2.cdsTransportador.Active := True;
      Consultar.Enabled := not DataModule2.cdsTransportador.IsEmpty;

      if  DataModule2.cdsTransportador.IsEmpty then
        SB_Consulta.SimpleText := Format('Nenhum registro for encontrado' + 'Com "%s"' , [Edit_Consulta.Text])
      else
        SB_Consulta.SimpleText := Format('%d registro encontrados com ' + 'Com "%s"' , [DataModule2.cdsTransportador.RecordCount, Edit_Consulta.Text]);
end;

procedure TForm1.DBGrid1DblClick(Sender: TObject);
begin
    PageControl1.activepage := TabSheet_Cadastro;
    Gravar.Enabled := True;
    Excluir.Enabled := True;
end;

procedure TForm1.ExcluirClick(Sender: TObject);
begin
  if DataSource1.DataSet.State in [dsBrowse, dsEdit] then
  begin
    DataModule2.cdsTransportador.Delete;
    DataModule2.cdsTransportador.ApplyUpdates(-1);
    DataModule2.cdsTransportador.Close;
  end
  else
    ShowMessage('Aten��o: Favor localizar o Registro a ser Excluido.');

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
      PageControl1.activepage := TabSheet_Consulta;
end;

procedure TForm1.GravarClick(Sender: TObject);
begin
if PageControl2.ActivePageIndex = 0 then  //Transportador Cadastro
begin
  if (DBEdit_Codigo.Field.AsString = '') or (DBEdit_Nome.Field.AsString = null) then
  begin
        if DataModule2.cdsTransportador.State in [dsInsert] then
        begin
            if (DBEdit_Nome.Field.AsString = '') or (DBEdit_Nome.Field.AsString = null) then
              begin
              ShowMessage('Aten��o: Favor Informar um Nome.');
              end
            else
              DataModule2.cdsTransportador.FieldByName('TR_CODIGO').AsInteger := ProximoID('GEN_TRANSPORTADORAS_ID');
              DataModule2.cdsTransportador.Post;
              DataModule2.cdsTransportador.ApplyUpdates(-1);
              //DataModule2.cdsTransportador.Close;
              Novo.Enabled := True;
              Novo.SetFocus;
        end
          else
            ShowMessage('Aten��o: Favor efetuar um cadastro.');
  end
    else
      if DataModule2.cdsTransportador.State in [dsEdit] then
        begin
          DataModule2.cdsTransportador.Post;
          DataModule2.cdsTransportador.ApplyUpdates(-1);
          //DataModule2.cdsTransportador.Close;
          Novo.Enabled := True;
          Novo.SetFocus;
        end
      else
        ShowMessage('Aten��o: Nao foi Editado o Cadastro.');
end
  else if PageControl2.ActivePageIndex = 1 then  //Placas Cadastros
if (DBEdit_Placa.Field.AsString = '') or (DBEdit_Placa.Field.AsString = null) then
  begin
        if DataModule2.cdsPlacas.State in [dsInsert] then
        begin
            if (DBEdit_Placa.Field.AsString = '') or (DBEdit_Placa.Field.AsString = null) then
              begin
              ShowMessage('Aten��o: Favor Informar uma Placa.');
              end
            else
              //DataModule2.cdsPlacas.FieldByName('TR_CODIGO').AsInteger := ProximoID('GEN_TRANSPORTADORAS_ID');
              DataModule2.cdsPlacas.Post;
              DataModule2.cdsPlacas.ApplyUpdates(-1);
              //DataModule2.cdsPlacas.Close;
              Novo.Enabled := True;
              Novo.SetFocus;
        end
          else
            ShowMessage('Aten��o: Favor efetuar um cadastro.');
  end
    else
      if DataModule2.cdsPlacas.State in [dsEdit] then
        begin
          DataModule2.cdsPlacas.Post;
          DataModule2.cdsPlacas.ApplyUpdates(-1);
          //DataModule2.cdsPlacas.Close;
          Novo.Enabled := True;
          Novo.SetFocus;
        end
end;



procedure TForm1.NovoClick(Sender: TObject);
begin
  Novo.Enabled := false;
  Gravar.Enabled := true;
  Cancelar.Enabled := true;

  PageControl1.activepage := TabSheet_Cadastro;
  PageControl2.activepage := TabSheet_Transportadora;

   DataModule2.cdsTransportador.Open;
   DataModule2.cdsTransportador.Append;
   Gravar.SetFocus;

end;

function ProximoID (GENERATOR: String): Integer;
begin
    DataModule2.sqlGen.Close;
    DataModule2.sqlGen.SQL.Clear;
    DataModule2.sqlGen.SQL.Add ('SELECT GEN_ID('+GENERATOR+',1) AS ID_ATUAL FROM RDB$DATABASE');
    DataModule2.sqlGen.Open;
    Result:=DataModule2.sqlGen.FieldByName('ID_ATUAL').AsInteger;
    DataModule2.sqlGen.Close;
end;

end.
