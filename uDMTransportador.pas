unit uDMTransportador;

interface

uses
  System.SysUtils, System.Classes, Data.DBXFirebird, Data.FMTBcd,
  Datasnap.DBClient, Datasnap.Provider, Data.DB, Data.SqlExpr;

type
  TDataModule2 = class(TDataModule)
    SQLConnection1: TSQLConnection;
    sqldTransportador: TSQLDataSet;
    dspTransportador: TDataSetProvider;
    cdsTransportador: TClientDataSet;
    sqldTransportadorTR_CODIGO: TIntegerField;
    sqldTransportadorTR_NOME: TStringField;
    sqldTransportadorTR_CPF: TStringField;
    sqldTransportadorTR_CGC: TStringField;
    cdsTransportadorTR_CODIGO: TIntegerField;
    cdsTransportadorTR_NOME: TStringField;
    cdsTransportadorTR_CGC: TStringField;
    sqlGen: TSQLQuery;
    sqldPlacas: TSQLDataSet;
    dspPlacas: TDataSetProvider;
    cdsPlacas: TClientDataSet;
    sqldPlacasPL_TRANSP: TIntegerField;
    sqldPlacasPL_PLACA: TStringField;
    sqldPlacasPL_DESCRICAO: TStringField;
    cdsPlacasPL_TRANSP: TIntegerField;
    cdsPlacasPL_PLACA: TStringField;
    cdsPlacasPL_DESCRICAO: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DataModule2: TDataModule2;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
